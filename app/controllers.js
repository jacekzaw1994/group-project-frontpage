angular.module('app.controllers', [])
    .controller('homeController', ['$scope', '$http', function ($scope, $http) {
        $scope.message = 'Everyone come and see how good I look!';

    }])
    .controller('aboutController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.message = 'Look! I am an about page.';
    }])
    .controller('contactController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.message = 'Contact us! JK. This is just a demo.';
    }])
    .controller('loginController', ['$rootScope', '$scope', '$http', '$location', '$routeParams', function ($rootScope, $scope, $http, $location, $routeParams) {

        var authenticate = function (credentials, callback) {

            var headers = credentials ? {
                authorization: "Basic "
                + btoa(credentials.username + ":" + credentials.password)
            } : {};

            $http.get('http://localhost:8080/login', {headers: headers}).success(function () {
                $rootScope.authenticated = true;
                console.log('zalogowany 1');
                
                $location.path("/creator");
                callback && callback();
            }).error(function () {
                $rootScope.authenticated = false;
                console.log("zdupcone 1");
                callback && callback();
            });

        };

        authenticate();
        $scope.credentials = {};
        $scope.login = function () {
            authenticate($scope.credentials, function () {
                if ($rootScope.authenticated) {
                    console.log('zalogowany 2');
                    $location.path("/creator");
                    $scope.error = false;
                } else {
                    console.log("zdupcone 2");
                    $scope.error = true;
                }
            });
        };

    }])
    .controller('creatorController', ['$rootScope', '$scope', '$http', '$location', '$routeParams', function ($rootScope, $scope, $http, $location, $routeParams) {

        $('#loader').hide();
        $('#schedule-area').hide();
        var days = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        var weeks = days;
        for (var i = 0; i < 4; i++) {
            weeks = weeks.concat(days);
        }
        $scope.columnName = weeks;
        $scope.contents = null;
        $scope.createScheduler = function () {
            $('#schedule-area').hide();
            $('#loader').show();
            $http.get('http://localhost:8080/schedule')
                .success(function (data) {
                    $('#loader').hide();
                    $scope.contents = data;
                    angular.forEach($scope.contents, function(value, key) {
                        console.log(value + '   ' + key);
                    });
                    $('#schedule-area').show();
                })
                .error(function (data, status, error, config) {
                    $scope.contents = [{heading: "Error", description: "Could not load json   data"}];
                });
        };


    }])
    .controller('scheduleController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.message = 'Schedule';
    }]);
