angular.module('app', [
    'ngRoute',
    'app.controllers',
    'routeStyles',
    'datatables'
]).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'pages/home.html',
        controller: 'homeController',
        css: ['assets/css/home/home-styles.css']
    }).when('/about', {
        templateUrl: 'pages/about.html',
        controller: 'aboutController',
        css: ['assets/css/about/about-styles.css', 'assets/css/about/about-second-styles.css']
    }).when('/contact', {
        templateUrl: 'pages/contact.html',
        controller: 'contactController',
        css: 'assets/css/home/home-styles.css'
    }).when('/login', {
        templateUrl: 'pages/login.html',
        controller: 'loginController',
        css: ['assets/css/login/form-elements.css', 'assets/css/login/form-styles.css']
    }).when('/creator', {
        templateUrl: 'pages/creator.html',
        controller: 'creatorController',
        css: ['assets/css/creator/creator-styles.css', 'assets/css/loaders.css', 'assets/css/creator/button-styles.css', 'assets/css/creator/bounced-arrow.css']
    }).when('/schedule', {
        templateUrl: 'pages/schedule.html',
        controller: 'scheduleController',
        css: 'assets/css/schedule/schedule-styles.css'
    }).otherwise({
        redirectTo: '/'
    });
}]);